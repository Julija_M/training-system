package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Course;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EmployeeRepository {

    public List<Course> fetchAllEmployees() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllDeptsHQL = "from Employee";
        Query query = session.createQuery(selectAllDeptsHQL);
        List<Course> list = query.list();
        return list;
    }

    public List<Course> fetchAllEmployeesCriteria() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Course> cr = criteriaBuilder.createQuery(Course.class);
        Root<Course> from = cr.from(Course.class);
        CriteriaQuery<Course> select = cr.select(from);
        Query<Course> query = session.createQuery(cr);
        return query.getResultList();
    }

    public static Course findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Course p = session.find(Course.class, id);
        session.close();
        return p;
    }

    public static Course save(Course course) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(course);
        transaction.commit();
        session.close();
        return course;
    }

    public static Course update(Course course) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(course);
        transaction.commit();
        session.close();
        return course;
    }

    public static void delete(Course Course) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Course);
        transaction.commit();
        session.close();
    }

}
