package lv.sda;

import lv.sda.entity.Feedback;
import lv.sda.repository.ProjectRepository;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ProjectRepositoryTest {

    @Test
    public void testFindById() {
        Feedback feedback = ProjectRepository.findById(2);
        assertThat(feedback.getDescription(), is("Java - Fitness Web App"));
    }

    @Test
    public void testProjectSaveAndUpdate() {
        Feedback p = new Feedback();
        p.setDescription("Hibernate at work");
        ProjectRepository.save(p);
        assertNotNull(p.getProjectId());
        p.setDescription("Hibernate at work updated");
        ProjectRepository.update(p);
        Feedback feedback = ProjectRepository.findById(p.getProjectId());
        assertThat(feedback.getDescription(), is("Hibernate at work updated"));
    }

    @Test
    public void testProjectDelete() {
        Feedback p = new Feedback();
        p.setDescription("Hibernate at work");
        ProjectRepository.save(p);
        assertNotNull(p.getProjectId());
        ProjectRepository.delete(p);
        Feedback feedback = ProjectRepository.findById(p.getProjectId());
        assertNull(feedback);
    }

}
