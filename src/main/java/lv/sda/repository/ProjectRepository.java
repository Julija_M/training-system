package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Feedback;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ProjectRepository {

    public static Feedback findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Feedback p = session.find(Feedback.class, id);
        session.close();
        return p;
    }

    public static Feedback save(Feedback feedback) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(feedback);
        transaction.commit();
        session.close();
        return feedback;
    }

    public static Feedback update(Feedback feedback) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(feedback);
        transaction.commit();
        session.close();
        return feedback;
    }

    public static void delete(Feedback feedback) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(feedback);
        transaction.commit();
        session.close();
    }

}
