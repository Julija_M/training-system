package lv.sda.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "projects")
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer projectId;
    private String description;

    @ManyToMany(mappedBy = "projects")
    private Set<Course> courses;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
