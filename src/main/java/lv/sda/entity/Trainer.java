package lv.sda.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "training-system_trainer")
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer trainerId;
    @Column(name = "name")

    @Column(name = "trainerName")
    private String trainerName;

    @Column(name = "trainerSurname")
    private String surname;

    @Column(name = "trainerEmail")
    private String email;

    public Integer getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(Integer trainerId) {
        this.trainerId = trainerId;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
