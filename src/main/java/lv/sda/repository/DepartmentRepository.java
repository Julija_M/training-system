package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DepartmentRepository {

    public static Trainer findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Trainer p = session.find(Trainer.class, id);
        session.close();
        return p;
    }

    public static Trainer save(Trainer Trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(Trainer);
        transaction.commit();
        session.close();
        return Trainer;
    }

    public static Trainer update(Trainer Trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(Trainer);
        transaction.commit();
        session.close();
        return Trainer;
    }

    public static void delete(Trainer Trainer) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Trainer);
        transaction.commit();
        session.close();
    }

}
